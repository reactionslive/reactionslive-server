
var system = require('system');
var fs = require('fs');
var args = system.args;
var pageurl = args[1];

var page = require('webpage').create();
page.viewportSize = { width: Number(args[2]), height: Number(args[3]) };
page.clipRect = {top:0, left:0, width: Number(args[2]), height: Number(args[3])  };

page.open(pageurl, function (status) {
  console.log(status)
  setInterval(function() {   
    page.render('/dev/stdout', { format: "jpg" , quality: "100"});
  }, 1000);
});
// http://www.goodboydigital.com/pixijs/examples/12-2/
