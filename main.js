var express = require('express');
var passport = require('passport');
var Strategy = require('passport-facebook').Strategy;
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mongoose = require('mongoose');
var MongoStore = require('connect-mongo')(session);
var request = require('request');

if(process.env.NODE_ENV =='production'){
    console.log('In production mode')
    var workerhost = 'http://ReactionsLive-loadbalancer-585730924.eu-west-1.elb.amazonaws.com:5000';
    var hostname = 'http://52.212.34.208:3000';
    var webhost = 'http://www.reactionslive.com';
}
else if(process.env.NODE_ENV =='local_web'){
    console.log('In local_web mode')
    var workerhost = 'http://ReactionsLive-loadbalancer-585730924.eu-west-1.elb.amazonaws.com:5000';
    var hostname = 'http://52.212.34.208:3000';
    var webhost = "http://localhost:9000";
}
else{
    console.log('In local mode')
    var workerhost ='http://localhost:5000';
    var hostname = "http://localhost:3000";
    var webhost = "http://localhost:9000";
}

//Configure MongoDB
mongoose.connect('mongodb://localhost:27017/reactionslive');
var Campaign = mongoose.model('Campaign', { template: String,
                                            name: String ,
                                            title: String,
                                            numFields: Number,
                                            fields: [{text: String, input: String, image: String}],
                                            background: String,
                                            pageID: String,
                                            postID: String,
                                            access_token: String
                                        });

// Create a new Express application.
var app = express();

app.use(cookieParser());
app.use(bodyParser.json());
app.use(session({ 
    secret: 'wow so secret', 
    resave: true, 
    saveUninitialized: true,
    store: new MongoStore({mongooseConnection: mongoose.connection})  }));
app.use(passport.initialize());
app.use(passport.session());



// Use application-level middleware for common functionality, including
// logging, parsing, and session handling.
// app.use(require('morgan')('combined'));
// app.use(require('body-parser').urlencoded({ extended: true }));
// app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));


//Allow the incoming calls we need
app.use(function(req, res, next) {
  var allowedOrigins = ['http://reactionslive.com','http://www.reactionslive.com', 'http://localhost:9000'];
  var origin = req.headers.origin;
  if(allowedOrigins.indexOf(origin) > -1){
       res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Methods', 'GET, OPTIONS, POST');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
  res.header('Access-Control-Allow-Credentials', true);
  return next();
});

// Initialize Passport and restore authentication state, if any, from the
// session.
// app.use(passport.initialize());
// app.use(passport.session());

// Configure the Facebook strategy for use by Passport.
//
// OAuth 2.0-based strategies require a `verify` function which receives the
// credential (`access_token`) for accessing the Facebook API on the user's
// behalf, along with the user's profile.  The function must invoke `cb`
// with a user object, which will be set at `req.user` in route handlers after
// authentication.
passport.use(new Strategy({
    clientID: '219165338509241', //process.env.CLIENT_ID,
    clientSecret: '737cab5339d55bf6443961bfc9a40473', //process.env.CLIENT_SECRET,
    callbackURL: hostname+'/login/callback',
    profileFields: ['id', 'displayName', 'photos', 'email']
  },
  function(access_token, refreshToken, profile, cb) {
    // In this example, the user's Facebook profile is supplied as the user
    // record.  In a production-quality application, the Facebook profile should
    // be associated with a user record in the application's database, which
    // allows for account linking and authentication with other identity
    // providers.
	console.log('returning profile')
    return cb(null, {
        'data': {displayName: profile.displayName,
                photoURL: profile.photos[0].value}, 
        'access_token': access_token});
  }));


// Configure Passport authenticated session persistence.
//
// In order to restore authentication state across HTTP requests, Passport needs
// to serialize users into and deserialize users out of the session.  In a
// production-quality application, this would typically be as simple as
// supplying the user ID when serializing, and querying the user record by ID
// from the database when deserializing.  However, due to the fact that this
// example does not have a database, the complete Facebook profile is serialized
// and deserialized.
passport.serializeUser(function(user, cb) {
  cb(null, user);
});

passport.deserializeUser(function(obj, cb) {
  return cb(null, obj);
});


// Passport+Facebook login and callback routes
app.get('/login',
    passport.authenticate('facebook', {scope:[
    'manage_pages', 
    'publish_pages', 
    'public_profile'
    ]})
  );

app.get('/login/callback',
  passport.authenticate('facebook', { failureRedirect: '/login' }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect(webhost+'/#/choose');
  });

//Passport logout route
app.get('/logout', function(req, res){
    req.logout();
    res.redirect(webhost+"/#/")
});

//Return user data if logged in, '0' otherwise
app.get('/loggedin', function(req, res){ 
    res.send(req.isAuthenticated() ? req.user.data : '0'); 
});



// app.get('/profile',
//   require('connect-ensure-login').ensureLoggedIn(),
//   function(req, res){
//     res.render('profile', { user: req.user });
//   });


// All the FB graph stuff ------------------------------
var fb = require('fbgraph');

app.get('/pages', function(req,res){

    fb.get('me/accounts'+'?access_token='+req.user.access_token, function(error, response){
        if(error){
            res.send(error);
        }
        else{
            res.send(response.data.map(function(el){return {
                name: el.name,
                id: el.id
            }}));
        }
    })

})

app.post('/startStream', function(req,res){
    if(req.user){
        var data = {status: "LIVE_NOW"};

        if(req.body.description){data.description = req.body.description;}
        if(req.body.title){data.title = req.body.title;}

        //First get the page's access_token again and save to Campaign so we can pull reactions easily - Should save this in user profile!
        fb.get(req.body.pageID +'?fields=access_token&access_token='+req.user.access_token, function(err, response){

            //Now, save access_token and pageID in Campaign
            Campaign.update({ _id: req.body.campaignID}, {$set: {pageID: req.body.pageID, access_token: response.access_token}},
                function(err, response){
                    if(err){
                        console.log("Mongoose Error: ", err)
                    }
                })
            
            //And start the live video
            // console.log(response)
            fb.post(req.body.pageID+'/live_videos?access_token='+response.access_token, data,function(err, response){
                
                
                if(err){
                    console.log(err)
                    res.status(500).send('Error: '+err)
                }
                else{

                    fb.get(response.id+'?access_token='+req.user.access_token, function(err, response){
                        console.log("response when querying newly created video: ", response)

                        var postID = decodeURIComponent(response.embed_html).match(/\d+/g)[1];
                        console.log('extracted postID: ', postID)
                        Campaign.update({ _id: req.body.campaignID}, {$set: {postID: postID}},
                            function(err, response){
                                if(err){
                                    console.log("Mongoose Error: ", err)
                                }
                            })

                        
                    })
                    
                    request({
                        url: workerhost+'/run',
                        method: 'POST',
                        json: true,
                        body: {
                                pageurl: webhost+'/#/live/'+req.body.campaignID,
                                // pageurl: 'http://www.goodboydigital.com/pixijs/examples/12-2/',
                                streamkey: response.stream_url.split('rtmp/')[1],
                                duration: req.body.duration,
                                width: req.body.width ? req.body.width : 476,
                                height: req.body.height ? req.body.height : 298
                        }
                    }, function(error, response, body){
                        console.log('body', body);
                        console.log('error', error);
                        res.send(body)
                    });

                }
            });

        })
    }
    else{
        res.send('you need to log in to start a stream!')
    }
    
})


//Get the reactions for a campaign
var reactions = ['LIKE', 'LOVE', 'WOW', 'HAHA', 'SAD', 'ANGRY'].map(function (e) {
    var code = 'reactions_' + e.toLowerCase();
    return 'reactions.type(' + e + ').limit(0).summary(total_count).as(' + code + ')'
}).join(',');

app.get('/reactionCounts', function(req,res){

    Campaign.findById(req.query._id, function(err, campaign){

        if(err){
            res.status(500).send('Error: '+err);
        }
        else if(!campaign.postID){
            res.status(500).send('Not live yet!')
        }
        else{
            fb.get(campaign.postID + '?fields=' + reactions + '&access_token=' + campaign.access_token, 
                function(err, response){

                    var reactioncounts={
                        like : response.reactions_like.summary.total_count,
                        love : response.reactions_love.summary.total_count,
                        wow : response.reactions_wow.summary.total_count,
                        haha : response.reactions_haha.summary.total_count,
                        sad : response.reactions_sad.summary.total_count,
                        angry : response.reactions_angry.summary.total_count
                    }
                    res.send(reactioncounts);
                });
        }

    })    

})

app.get('/comments', function(req, res){
    Campaign.findById(req.query._id, function(err, campaign){

        if(err){
            res.status(500).send('Error: '+err);
            console.log(err)
        }
        else if(!campaign.postID){
            res.status(500).send('Not live yet!')
        }
        else{
            fb.get(campaign.postID + '/comments?access_token=' + campaign.access_token, 
                function(err, response){
                    
                    res.send(response.data);
                });
        }

    })
})


// All the AWS upload stuff -------------------
var aws = require('aws-sdk');

aws.config.update({
  accessKeyId: 'AKIAJ5Q6LSTXCOQALKZQ', //process.env.AWS_ACCESS_KEY,
  secretAccessKey: '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', //process.env.AWS_SECRET_KEY,
  signatureVersion: 'v4',
  region: 'eu-west-1'
});

app.get('/getSignedUrl', function(req,res){

    var s3 = new aws.S3();
    var s3_params = {
      Bucket: 'reactionslive',
      Key: req.query.filename,
      Expires: 60,
      ACL: 'public-read'
    };
    s3.getSignedUrl('putObject', s3_params, function (err, signedUrl) {
        res.send(signedUrl)
    });
})

// --------------------------------

app.post('/campaign', function(req,res){

  Campaign.create(req.body, function(err, campaign){
    if(err){
        res.status(500).send("Mongoose Error: "+err)
    }
    else{
        if(campaign.access_token){delete campaign.access_token;}
        res.send(campaign)
    }
  })

})

app.get('/campaign', function(req, res){

    Campaign.findById(req.query._id, function(err, campaign){
        if(err){
            res.status(500).send('Error: '+err);
        }
        else{
            if(campaign.access_token){delete campaign.access_token;}
            res.send(campaign);
        }
        
    })

})

// Emailing stuff ---------------

var nodemailer = require('nodemailer');

app.post('/email', function(req, res){
    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport('smtps://reactionslivemailer%40gmail.com:reactionsliv@smtp.gmail.com');

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: '"'+req.body.email+'" <foo@blurdybloop.com>',//Don't know why I can't put the right email
        replyTo: req.body.email,
        to: 'info@reactionslive.com', // list of receivers
        subject: 'Contact form', // Subject line
        text: req.body.text // plaintext body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log(error)
        }
        res.send('Message sent: ' + info.response)
    });  

})

// Stripe stuff -------------------

app.post('/stripe', function(req,res){
    res.send(req.body);
})


app.listen(3000);
