var express = require('express')
var app = express();
var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
var kill = require('tree-kill');

var bodyParser = require("body-parser");    
app.use(bodyParser.json());

app.use(function(req,res,next){
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'GET,POST');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Authorization'); 

  next();
});

//Receive query to run video
app.post('/run', function(req,res){


  // console.log('Launching new video:')
  // console.log('page: '+req.body.pageurl,'\n', 'streamkey: '+req.body.streamkey)

  var execcommand = "phantomjs phantom.js "+req.body.pageurl+" " + req.body.width+" " + req.body.height+
  " | ffmpeg -loglevel quiet -r 1 -c:v mjpeg -f image2pipe -i - -i ./assets/Silence4hours.mp3 -c:v libx264 -x264-params keyint=1 -r 1 -pix_fmt yuv420p -strict experimental -c:a aac -b:a 1k -f flv 'rtmp://rtmp-api.facebook.com:80/rtmp/"+req.body.streamkey+"'";
  console.log(execcommand); 
  var child = exec(execcommand, function(error, stdout, stderr){
    if(error){
      // console.log(error);
    }
    if(stderr){
      // console.log(stderr.toString());
    }
  });


  //Kill the processes after a set time (in minutes, default 30)
  var duration = req.body.duration ? req.body.duration : 30
  setTimeout(function(){
    console.log('Stream finished: '+req.body.streamkey);
    kill(child.pid);

  },1000*60*duration);

 
  res.send('Campaign started: '+req.body.streamkey+', duration: '+req.body.duration)

});

app.post('/test', function(req, res){
  console.log(req.body)
  res.send('This is the response: \n'+JSON.stringify(req.body));
})

app.get('/ping', function(req, res){
  res.sendStatus(200);
});

app.listen(5000);
console.log("Listening on port 5000")


// //Kill any child processes still running when node wants to exit
// process.on('exit', function() {
//   console.log('killing', children.length, 'child processes');
//   children.forEach(function(child) {
//     // child.kill();
//     kill(child.pid);
//   });
// });

// //Make sure node exits cleanly if terminated externally
// var cleanExit = function() { process.exit() };
// process.on('SIGINT', cleanExit); // catch ctrl-c
// process.on('SIGTERM', cleanExit); // catch kill




